FROM ubuntu:14.04

RUN apt-get update -y && \
  apt-get upgrade -y

RUN apt-get install keepalived -y

ADD entrypoint.sh /
ADD keepalived/ /etc/keepalived/
ADD config_keepalived.sh /
ADD assign-ip.py /usr/local/bin/
ADD chk_service.sh /usr/local/bin/

RUN chmod +x /config_keepalived.sh
RUN chmod +x /entrypoint.sh
RUN chmod +x /etc/keepalived/master.sh
RUN chmod +x /usr/local/bin/assign-ip.py
RUN chmod +x /usr/local/bin/chk_service.sh

RUN apt-get update && apt-get install -y \
  dnsutils \
  curl \
  python3-pip

#startup script
ENTRYPOINT ["/entrypoint.sh"]
