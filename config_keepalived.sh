#!/bin/bash
set -e

echo "=> Configuring Keepalived"
sed -i -e "s/<--PRIMARY_IP-->/${PRIMARY_IP}/g" /etc/keepalived/keepalived.conf
sed -i -e "s/<--SECONDARY_IP-->/${SECONDARY_IP}/g" /etc/keepalived/keepalived.conf
sed -i -e "s/<--STATE-->/${STATE}/g" /etc/keepalived/keepalived.conf
sed -i -e "s/<--PRIORITY-->/${PRIORITY}/g" /etc/keepalived/keepalived.conf

sed -i -e "s/<--TOKEN-->/${TOKEN}/g" /etc/keepalived/master.sh
sed -i -e "s/<--FLOATING_IP_ADDRESS-->/${FLOATING_IP_ADDRESS}/g" /etc/keepalived/master.sh

echo "=> Done!"
