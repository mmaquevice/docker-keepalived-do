#!/bin/bash

NB_SERVICES=$(dig @172.17.0.1 $1.service.consul +short +time=1 +tries=1 | wc -l)

if [ ${NB_SERVICES} -gt 0 ]
then
  exit 0
fi

exit 1