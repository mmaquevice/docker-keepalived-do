A simple keepalived container to provide a vip on docker containers.

Run with:

sed -i -e "s/<--PRIMARY_IP-->/${PRIMARY_IP}/g" /etc/keepalived/keepalived.conf
sed -i -e "s/<--SECONDARY_IP-->/${SECONDARY_IP}/g" /etc/keepalived/keepalived.conf
sed -i -e "s/<--STATE-->/${STATE}/g" /etc/keepalived/keepalived.conf
sed -i -e "s/<--PRIORITY-->/${PRIORITY}/g" /etc/keepalived/keepalived.conf

sed -i -e "s/<--TOKEN-->/${TOKEN}/g" /etc/keepalived/master.sh
sed -i -e "s/<--FLOATING_IP_ADDRESS-->/${FLOATING_IP_ADDRESS}/g" /etc/keepalived/master.sh

```
docker run -d  --net=host --privileged=true --name keepalived1 \
-e PRIMARY_IP==10.0.0.1 -e SECONDARY_IP==10.0.0.2 \
-e STATE==MASTER -e PRIORITY==200 \
-e TOKEN==1234 -e FLOATING_IP_ADDRESS==182.0.0.1 \
mmaquevice/keepalived-do
```
