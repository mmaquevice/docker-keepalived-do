#!/bin/bash

/config_keepalived.sh
/usr/sbin/keepalived -D --dont-fork --log-console -f /etc/keepalived/keepalived.conf
