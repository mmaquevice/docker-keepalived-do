#!/usr/bin/env bash

export DO_TOKEN='<--TOKEN-->'
IP='<--FLOATING_IP_ADDRESS-->'
ID=$(curl -s http://169.254.169.254/metadata/v1/id)
HAS_FLOATING_IP=$(curl -s http://169.254.169.254/metadata/v1/floating_ip/ipv4/active)

if [ $HAS_FLOATING_IP = "false" ]; then
    n=0
    while [ $n -lt 10 ]
    do
        python3 /usr/local/bin/assign-ip.py $IP $ID && break
        n=$((n+1))
        sleep 3
    done
fi